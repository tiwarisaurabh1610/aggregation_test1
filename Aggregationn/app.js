const express=require('express');
const connection=require('./Connection');
const routerC=require('./Router/Collegeroute');
const routerS=require('./Router/Studentroute');
const routerM=require('./Router/Marksroute');
require('dotenv').config();
const port = process.env.PORT || 3200;
const app=express();

app.use(express.json());
app.use(express.urlencoded({extended:true}));
connection.then(()=>{
    app.listen(port,()=>{
        console.log(`listening on port ${port}`);
    })
}).catch(()=>{
    console.log("Error in connecting with database");
}) 
app.use('/Student',routerS);
app.use('/Marks',routerM);
app.use('/College',routerC);